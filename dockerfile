FROM hasura/graphql-engine:v1.2.2

COPY gudea.css /srv/console-assets/common/css/gudea.css
COPY roboto.css /srv/console-assets/common/css/roboto.css
COPY neIFzCqgsI0mp9CG_oC-Nw.woff2 /srv/console-assets/common/fonts/neIFzCqgsI0mp9CG_oC-Nw.woff2
COPY neIFzCqgsI0mp9CI_oA.woff2 /srv/console-assets/common/fonts/neIFzCqgsI0mp9CI_oA.woff2
COPY neIIzCqgsI0mp9gz25WBFqw.woff2 /srv/console-assets/common/fonts/neIIzCqgsI0mp9gz25WBFqw.woff2
COPY neIIzCqgsI0mp9gz25WPFqwKUQ.woff2 /srv/console-assets/common/fonts/neIIzCqgsI0mp9gz25WPFqwKUQ.woff2
COPY roboto/ /srv/console-assets/common/fonts/roboto/
COPY main-1.2.css.gz /srv/console-assets/versioned/main.css.gz
COPY main-1.2.js.gz /srv/console-assets/versioned/main.js.gz
COPY vendor-1.2.js.gz /srv/console-assets/versioned/vendor.js.gz
COPY voyager.worker.min.js /srv/console-assets/
RUN cd /srv/console-assets/versioned && \
    gzip -f main.css.gz && \
    mv main.css.gz.gz main.css.gz && \
    gzip -f main.js.gz && \
    mv main.js.gz.gz main.js.gz && \
    gzip -f vendor.js.gz && \
    mv vendor.js.gz.gz vendor.js.gz

RUN addgroup -S hasuraGroup && \
    adduser -S hasura -G hasuraGroup && \
    chown hasura /srv

USER hasura